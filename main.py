#!/usr/bin/python3

import os
import argparse

def parse_args():
    parser = argparse.ArgumentParser(description='Find files')
    parser.add_argument('-r', '--regex', action='store_true')
    parser.add_argument('-v', '--verbose', action='count', default=0)
    parser.add_argument('pattern', type=str, help='search pattern')
    parser.add_argument('path', type=str, nargs='?', help='search path', default='.')

    return parser.parse_args()

def main():
    args = parse_args();

    if args.regex:
        command = 'find {path} -type f -regex  \"{pattern}\"'.format(**vars(args))
    elif '*' in args.pattern:
        command = 'find {path} -type f -iname  \"{pattern}\"'.format(**vars(args))
    else:
        command = 'find {path} -type f -iname \"*{pattern}*\"'.format(**vars(args))

    if args.verbose > 2:
        print(command)

    os.system(command)

    return 0

if __name__ == '__main__':
    exit(main())
